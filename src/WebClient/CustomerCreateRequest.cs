using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebClient
{
    public class CustomerCreateRequest
    {
        public string Firstname { get; init; }

        public string Lastname { get; init; }

        public CustomerCreateRequest()
        {
        }

        public CustomerCreateRequest(
            string firstName,
            string lastName)
        {
            Firstname = firstName;
            Lastname = lastName;
        }

        public async Task<string> Send(HttpClient client, string requestUri)
        {
            var customer = new Customer { Firstname = Firstname, Lastname = Lastname };
            string json = JsonConvert.SerializeObject(customer);
            HttpContent content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            var responseMessage = await client.PostAsync(requestUri, content);

            string msg;
            try
            {
                msg = await responseMessage.Content.ReadAsStringAsync();
            }
            catch (System.Net.Http.HttpRequestException ex)
            {
                msg = ex.Message;
            }

            return msg;
        }
    }
}