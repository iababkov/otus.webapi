﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        private static readonly HttpClient client = new();

        static async Task Main()
        {
            UriBuilder uriBuilder = new("https", "localhost", 5001, "customers");

            string msg = await Program.RandomCustomer().Send(client, uriBuilder.ToString());

            if (long.TryParse(msg, out long replyId))
            {
                await GetCustomer(client, uriBuilder, replyId);
            }
            else
            {
                Console.WriteLine(msg);
            }

            while (true)
            {
                Console.Write("Enter customer Id to request from server: ");
                string input = Console.ReadLine();
                if (input == "exit")
                {
                    break;
                }

                if (long.TryParse(input, out long id) && id > 0)
                {
                    await GetCustomer(client, uriBuilder, id);
                }
                else
                {
                    Console.WriteLine("Customer Id must be a positive integer!");
                }
            }
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            NameGenerator.Generators.RealNameGenerator nameGenerator = new();

            string name = nameGenerator.Generate();
            var nameArray = name.Split(" ", 2);

            return new CustomerCreateRequest(nameArray[0], nameArray[1]);
        }

        private static async Task<string> GetCustomer(HttpClient client, UriBuilder uriBuilder, long customerId)
        {
            uriBuilder.Path = "customers/" + customerId;

            string reply = "";
            try
            {
                reply = await client.GetStringAsync(uriBuilder.ToString());

                Console.WriteLine($"Customer generated: {reply}");
            }
            catch (System.Net.Http.HttpRequestException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return reply;
        }
    }
}