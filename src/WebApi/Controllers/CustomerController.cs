using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly CustomerContext _context;

        public CustomerController(CustomerContext context)
        {
            _context = context;
        }

        [HttpGet("{id:long}")]
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] long id)
        {
            var customer = await _context.Customers.FindAsync(id);

            if (customer == null)
            {
                return NotFound(); // Status code 404
            }

            return Ok(customer); // Status code 200
        }

        [HttpPost("")]
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customer)
        {
            if (_context.Find<Customer>(customer.Id) is not null) {
                return Conflict(new { message = $"Customer with Id {customer.Id} already exists!"}); // Status code 409
            }

            _context.Customers.Add(customer);

            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customer.Id }, customer.Id); // Status code 201
        }
    }
}